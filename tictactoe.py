import struct, string
import copy

class TicTacToeBoard:

    def __init__(self):
        self.board = (['N']*3,['N']*3,['N']*3)
                                      
    def PrintBoard(self):
        print(self.board[0][0] + "|" + self.board[1][0] + "|" + self.board[2][0])
        
        print(self.board[0][1] + "|" + self.board[1][1] + "|" + self.board[2][1])
        
        print(self.board[0][2] + "|" + self.board[1][2] + "|" + self.board[2][2])
        
    def play_square(self, col, row, val):
        self.board[col][row] = val
        return self

    def get_square(self, col, row):
        return self.board[col][row]

    def full_board(self):
        for i in range(3):
            for j in range(3):
                if(self.board[i][j]=='N'):
                    return False

        return True

    def moves(self):
        m = []
        for i in range(3):
            for j in range(3):
                if self.board[i][j] == 'N':
                    m.append([i,j])
        return m
    
    #if there is a winner this will return their symbol (either 'X' or 'O'),
    #otherwise it will return 'N'
    def winner(self):
        #check the cols
        for col in range(3):
            if(self.board[col][0]!='N' and self.board[col][0] == self.board[col][1] and self.board[col][0]==self.board[col][2] ):
                return self.board[col][0]
        #check the rows
        for row in range(3):
            if(self.board[0][row]!='N' and self.board[0][row] == self.board[1][row] and self.board[0][row]==self.board[2][row] ):
                return self.board[0][row]
        #check diagonals
        if(self.board[0][0]!='N' and self.board[0][0] == self.board[1][1] and self.board[0][0]==self.board[2][2] ):
            return self.board[0][0]
        if(self.board[2][0]!='N' and self.board[2][0] == self.board[1][1] and self.board[2][0]==self.board[0][2]):
            return self.board[2][0]
        return 'N'

def make_simple_cpu_move(board, cpuval):
    for i in range(3):
        for j in range(3):
            if(board.get_square(i,j)=='N'):
                board.play_square(i,j,cpuval)
                return True
    return False

def alphabeta(board, depth, alpha, beta, cpuval):
    newboard = copy.deepcopy(board)
    if depth == 0 or board.full_board():
        return board
    if cpuval == 'O':
        for m in newboard.moves():
            newboard.play_square(m[0], m[1], cpuval)
            alpha = max(alpha, alphabeta(newboard, depth - 1, alpha, beta, 'X' if 'O' else 'X'))
            if beta <= alpha:
                break
        return alpha
    else:
        for m in newboard.moves():
            # newboard = copy.deepcopy(board)
            newboard.play_square(m[0], m[1], cpuval)
            beta = min(beta, alphabeta(newboard, depth - 1, alpha, beta, 'X' if 'O' else 'X'))
            if beta <= alpha:
                break
        return beta
        
        
def ab_decision(board, cpuval):
    resultList = []
    moves = board.moves()
    for m in moves:
        newboard = copy.deepcopy(board)
        newboard.play_square(m[0], m[1], cpuval)
        result = max_val(newboard, 999, -999, cpuval)
        resultList.append(result)
    bestmove = max(enumerate(resultList), key=lambda x: x[1])[0]
    board.play_square(moves[bestmove][0], moves[bestmove][1], cpuval)
    return board


def max_val(board, alpha, beta, cpuval):
    if board.winner() == cpuval:
        return 1
    elif board.winner() == 'X' if cpuval == 'O' else 'O':
        return -1
    elif board.full_board():
        return 0 
        
    
    v = -999
    for s in board.moves():
        
        v += max(v, max_val(board.play_square(s[0], s[1], cpuval), beta, alpha, cpuval))
        # print v
        if v >= beta: return v
        alpha = max(alpha, v)
    return v

def play_cpufirst():
    Board = TicTacToeBoard()
    humanval =  'O'
    cpuval = 'X'
    Board.PrintBoard()
    
    while( Board.full_board()==False and Board.winner() == 'N'):
        print("CPU Move")
        ab_decision(Board, cpuval)
        Board.PrintBoard()

        if(Board.full_board() or Board.winner()!='N'):
            break
        else:
            while(True):
                print("your move, pick a row (0-2)")
                row = int(input())
                print("your move, pick a col (0-2)")
                col = int(input())

                if(Board.get_square(col,row)!='N'):
                    print("square already taken!")
                else:
                    break
            Board.play_square(col,row,humanval)
                
    Board.PrintBoard()
    if(Board.winner()=='N'):
        print("Cat game")
    elif(Board.winner()==humanval):
        print("You Win!")
    elif(Board.winner()==cpuval):
        print("CPU Wins!")

def play():
    Board = TicTacToeBoard()
    humanval =  'X'
    cpuval = 'O'
    Board.PrintBoard()
    
    while( Board.full_board()==False and Board.winner() == 'N'):
        print("your move, pick a row (0-2)")
        row = int(input())
        print("your move, pick a col (0-2)")
        col = int(input())

        if(Board.get_square(col,row)!='N'):
            print("square already taken!")
            continue
        else:
            Board.play_square(col,row,humanval)
            if(Board.full_board() or Board.winner()!='N'):
                break
            else:
                Board.PrintBoard()
                print("CPU Move")
                # make_simple_cpu_move(Board,cpuval)
                #alphabeta(Board, 9, -999, 999, cpuval)
                ab_decision(Board, cpuval)
                Board.PrintBoard()

    Board.PrintBoard()
    if(Board.winner()=='N'):
        print("Cat game")
    elif(Board.winner()==humanval):
        print("You Win!")
    elif(Board.winner()==cpuval):
        print("CPU Wins!")

def main():
    # play()
    play_cpufirst()

main()
            
