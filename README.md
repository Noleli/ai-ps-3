## To run

To run, just run `python tictactoe.py`.

To change who plays first, there are two functions that may be called from `main()`: `play()` and `play_cpufirst()`. The former lets you, the interactive player, play first; the latter — as the name suggests — lets the computer play first.

## System requirements

This has been tested on Python 2.7.2. I would expect it to run on anything over 2.5, but I haven't tested it.